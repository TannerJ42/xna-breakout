﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Breakout
{
    class Ball
    {
        Texture2D sprite;
        Rectangle drawRectangle;

        Color color = Color.Red; // this sets the color of the ball. It changes all non-transparent pixels to this color, so it won't work well if the ball is more than one color.

        // Movement controls
        float speed = 600f; // this sets the speed to 600 pixels per second. The f indicates that the number is a float, which means it can be a decimal.
        Vector2 position;
        Vector2 velocity = new Vector2(1, -1); // initial velocity


        /// <summary>
        /// A circle with a diameter of 20 pixels. When it impacts any object, it changes direction by 180 degrees.
        /// </summary>
        public Ball()
        {
            Console.WriteLine("Creating the ball.");
        }

        /// <summary>
        /// Load the sprite and create the drawRectangle based on the sprite's dimensions. Create a rectangle to show where it should be drawn.
        /// </summary>
        /// <param name="contentManager">Content manager which keeps track of this image.</param>
        internal void Load(ContentManager contentManager)
        {
            Console.WriteLine("Loading the ball.");

            // load the image
            sprite = contentManager.Load<Texture2D>("ball"); // this takes ball.png and saves it as a Texture2D called 'sprite'. If you change the name you must add the new image under BreakoutContent.

            // create a rectangle for the image
            drawRectangle = new Rectangle(500, 200, 20, 20); // this makes a rectangle with 20 width and 20 height, and places its top left at x=500, y=200.

            // initialize position to the starting location of the drawRectangle.
            position = new Vector2(drawRectangle.X, drawRectangle.Y);
        }

        /// <summary>
        /// Moves the ball and checks for collisions.
        /// </summary>
        /// <param name="gameTime">System clock.</param>
        public void Update(GameTime gameTime, Panel panel)
        {
            // Adjust the position based on velocity
            MoveBall(gameTime);

            // Bounce the ball if the panel touches it
            CheckForPanel(panel);

            // Check for bounces
            CheckForBorders();

            //// Move the area where the ball is drawn on screen.
            //drawRectangle.Y -= (int)(speed * gameTime.ElapsedGameTime.TotalSeconds); // Multiply the speed by the fraction of a second that has passed since last update, then lower the drawRectangle by that amount.
        }

        private void CheckForPanel(Panel panel) // Set ball.velocity.Y to negative if 
        {
            // for now, if panel intersects ball, ball velocity.Y is made negative.
            if (panel.drawRectangle.Intersects(this.drawRectangle))
            {
                velocity.Y = -Math.Abs(velocity.Y); // make the ball's velocity upward.
            }
        }

        private void MoveBall(GameTime gameTime)
        {
            // Move the ball based on its velocity and speed.
            position += velocity * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Set the drawRectangle to the position.
            SetDrawRectangleToPosition();
        }

        private void SetDrawRectangleToPosition()
        {
            drawRectangle.X = (int)position.X;
            drawRectangle.Y = (int)position.Y;
        }

        private void CheckForBorders() // keep ball inside left, right, and top borders of screen. Lose if it's outside bottom border.
        {
            if (drawRectangle.Top < 0) // Top of the ball is offscreen
            {
                velocity.Y = Math.Abs(velocity.Y); // Make the Y velocity positive. Remember, for Y, greater than 0 is down, less than 0 is up!
                //velocity.X = Math.Abs(velocity.X + 3);
            }
            if (drawRectangle.Left < 0) // Left side of the ball is offscreen
            {
                velocity.X = Math.Abs(velocity.X); // Make the X velocity positive. For X, lower than 0 is left, greater than 0 is right.
            }
            if (drawRectangle.Right > 800) // Right side of the ball is offscreen
            {
                velocity.X = -Math.Abs(velocity.X); // Make the X velocity negative. For X, lower than 0 is left, greater than 0 is right.
            }
            if (drawRectangle.Top > 600) // Top side of the ball is offscreen
            {
                LoseGame();
            }
        }

        private void LoseGame() // call this when it's time for the game to end.
        {
            // not yet implemented
            Console.WriteLine("Game over, man.");
        }

        /// <summary>
        /// Displays the ball's sprite in the ball's draw rectangle.
        /// </summary>
        /// <param name="spriteBatch">Draw process.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, drawRectangle, color);
        }


    }
}
