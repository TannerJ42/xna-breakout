﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Breakout
{
    class Panel
    {
        Texture2D sprite; // this is the image.
        public Rectangle drawRectangle; // this is where the image is drawn.

        Color color = Color.Blue; // this sets the color of the ball. It changes all non-transparent pixels to this color, so it won't work well if the ball is more than one color.

        // movement controls
        Vector2 position = new Vector2(); // this will hold the current position.
        Rectangle lastRoundRectangle; // this will hold the position from last round.

        /// <summary>
        /// Calling this will return a Vector2 with x == -1 if the panel is moving left and 1 if right.
        /// </summary>
        public Vector2 Velocity
        {
            get
            {
                if (position.X < lastRoundRectangle.X) // panel is moving left
                {
                    return new Vector2(-1, 0);
                }
                else if (position.X > lastRoundRectangle.X) // panel is moving right
                {
                    return new Vector2(1, 0);
                }
                else
                {
                    return new Vector2(); // if still, return a vector of (0, 0)
                }
            }
        }

        /// <summary>
        /// A circle with a diameter of 20 pixels. When it impacts any object, it changes direction by 180 degrees.
        /// </summary>
        public Panel()
        {
            Console.WriteLine("Creating the Panel.");
        }

        /// <summary>
        /// Load the sprite and create the drawRectangle based on the sprite's dimensions. Create a rectangle to show where it should be drawn.
        /// </summary>
        /// <param name="contentManager">Content manager which keeps track of this image.</param>
        internal void Load(ContentManager contentManager)
        {
            Console.WriteLine("Loading the Panel.");

            // load the image
            sprite = contentManager.Load<Texture2D>("panel"); // this takes panel.png and saves it as a Texture2D called 'sprite'. If you change the name you must add the new image under BreakoutContent.

            // create a rectangle for the image
            drawRectangle = new Rectangle(400, 570, 170, 70); // this makes a rectangle with 20 width and 20 height, and places its top left at x=500, y=200.
        }

        /// <summary>
        /// Moves the ball and checks for collisions.
        /// </summary>
        /// <param name="gameTime">System clock.</param>
        public void Update(GameTime gameTime)
        {
            // Grab the mouse coordinates and save them as (X, Y), or a Point.
            Point coords = new Point(Mouse.GetState().X, Mouse.GetState().Y);

            // save the current position as lastRoundRectangle.
            lastRoundRectangle = drawRectangle;

            // Move the area where the panel is drawn on screen.
            drawRectangle.X = coords.X - drawRectangle.Width / 2;

            // move the panel's position based on velocity.
            //position += velocity * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (drawRectangle.X < 0) // left border of panel is off screen to the left.
                drawRectangle.X = 0;
            else if (drawRectangle.X > 800 - drawRectangle.Width) // left border of panel is off screen to the right, adjusting for width of panel
                drawRectangle.X = 800 - drawRectangle.Width;
        }

        /// <summary>
        /// Displays the ball's sprite in the ball's draw rectangle.
        /// </summary>
        /// <param name="spriteBatch">Draw process.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, drawRectangle, color);
        }

    }
}
